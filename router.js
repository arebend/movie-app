const router = require('express').Router();

const success = require('./middlewares/success')
const authentication = require('./middlewares/authentication')
const authorizationUser = require('./middlewares/authorizationUser')
const isAdmin = require('./middlewares/isAdmin')

const movieCheck = require('./middlewares/movieCheck')
const uploader = require('./middlewares/uploader');

const user = require('./controller/user');
const movie = require('./controller/movie')
const review = require('./controller/review')

/* User API endpoint */
router.post('/user/register', user.register, success);
router.post('/user/login', user.login, success);
router.get('/user/profile', authentication, authorizationUser, user.userProfile, success);
router.put('/user/updateProfile', authentication, authorizationUser, uploader.single('image'), user.userUpdate, success)


/* Movie API endpoint  */
router.post('/movie/create/admin', movie.adminCreate, success) /* input one by one*/
router.post('/movie/create', movie.create, success); /* create movie in params by axios */
router.get('/movie/show', movie.showAll, success);
router.get('/movie/show/detail/:id', movie.detail, success);
router.get('/movie/show/search/', movie.search, success);


/* Review API endpoint */
router.post('/movie/review/create/:movies_id', authentication, review.create, success)
router.get('/movie/review/show/:movies_id', authentication, review.showAll, success);
router.put('/movie/review/update/:movies_id', authentication, movieCheck('Review'), review.update, success);
router.delete('/movie/review/delete/:movies_id', authentication, movieCheck('Review'), review.delete, success);

/* Admin API endpoint */
router.post('/login', user.adminLogin, success)

module.exports = router; 
