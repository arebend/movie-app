'use strict';
module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define('Review', {
    rating: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: {
          args: true,
          msg: "Please Input in Number"
        },
        max: {
          args: 5,
          msg: "Greater than 5"
        },
        min: {
          args: 1,
          msg: "Lesser than 1"
        }
      }
    },
    comment: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Comment must be filled'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'User ID Must be Filled'
        }
      }
    },
    movie_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'User ID Must be Filled'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Reviews'
  });
  Review.associate = function (models) {
    // associations can be defined here
    Review.belongsTo(models.User, {
      foreignKey: 'user_id',
      targetKey: 'id',
      as: 'reviewer'
    })
    Review.belongsTo(models.Movie, {
      foreignKey: 'movie_id',
      targetKey: 'id'
    })
  };
  return Review;
};