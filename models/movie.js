'use strict';
module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    title: DataTypes.STRING,
    trailer: DataTypes.TEXT,
    poster: DataTypes.TEXT,
    director: DataTypes.TEXT,
    genre: DataTypes.TEXT,
    release_date: DataTypes.STRING,
    actor: DataTypes.TEXT,
    production_house: DataTypes.STRING,
    synopsis: DataTypes.TEXT,
    languages: DataTypes.STRING
  }, {});
  Movie.associate = function (models) {
    // associations can be defined here
    Movie.hasMany(models.Review, {
      foreignKey: 'movie_id'
    })

  };
  return Movie;
};