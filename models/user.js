'use strict';
const bcrypt = require('../helpers/bcrypt');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Name must be filled'
        }
      }
    },
    email: {
      type: DataTypes.STRING,
      unique: {
        msg: "Please user another email"
      },
      validate: {
        notEmpty: {
          msg: "Email field is empty"
        },
        isEmail: {
          msg: "Please input with email format"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: "Password must be filled"
        },
        len: [2, 10],
        isAlphanumeric: {
          msg: "Password must be contain between alphabet and number"
        }
      }
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: 'member',
      validate: {
        notEmpty: {
          msg: 'Role must be filled'
        }
      }
    },
  }, {
    sequelize,
    tableName: 'Users',
    hooks: {
      beforeCreate: (user, options) => {
        user.password = bcrypt.hasher(user.password)
      }
    }
  });
  User.associate = function (models) {
    // associations can be defined here
    User.hasMany(models.Review, {
      foreignKey: 'user_id',
      sourceKey: 'id'
    }),
      User.hasOne(models.Profile, {
        foreignKey: 'profile_id',
        sourceKey: 'id'
      })
  };
  return User;
};