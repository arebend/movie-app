'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profile = sequelize.define('Profile', {
    image: {
      type: DataTypes.TEXT,
      defaultValue: "https://ik.imagekit.io/xx7jeqftvf/movie_app_V9gtO1ImCI.jpg",
      validate: {
        isUrl: {
          msg: 'Image must be Filled by URL'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'User ID Must be Filled'
        }
      }
    }
  }, {
    sequelize,
    tableName: 'Profiles'
  });
  Profile.associate = function (models) {
    // associations can be defined here
    Profile.belongsTo(models.User, {
      foreignKey: 'profile_id',
      targetKey: 'id'
    })
  };
  return Profile;
};