const { Review, Movie } = require('../models')

module.exports = {

    async create(req, res, next) {
        try {
            // console.log(req.payload.data.id, '  iniii useer iid  ');
            const movie = await Movie.findByPk(req.params.movies_id)
            // console.log(movie);

            const duplicate = await Review.findOne({
                where: {
                    movie_id: movie.id,
                    user_id: req.payload.data.id
                }
            })
            // console.log(duplicate);

            if (duplicate) {
                throw new Error('review already inputed')
            }
            const data = await Review.create({
                rating: req.body.rating,
                comment: req.body.comment,
                movie_id: movie.id,
                user_id: req.payload.data.id
            });

            res.data = data
            res.status(201);
            next();
        }
        catch (err) {
            console.log(err.message);
            res.status(422)
            next(err)
        }
    },

    async showAll(req, res, next) {
        try {
            const data = await Review.findAndCountAll({
                where: {
                    movie_id: req.params.movies_id
                },
                include: 'reviewer'
            });
            res.data = data
            res.status(201);
            next();
        } catch (err) {
            console.log(err.message);
            res.status(422)
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            const comment = req.query.comment
            if (comment) {
                const data = await Review.findAndCountAll({
                    limit: req.query.limit || 10,
                    offset: (req.query.page - 1) * req.query.limit || 0,
                    where: {
                        comment: {
                            [Op.substring]: comment
                        }
                    }
                });
                res.data = data
                res.status(200);
                next();
            } else {
                const data = await Review.findAndCountAll({
                    limit: req.query.limit || 10,
                    offset: (req.query.page - 1) * req.query.limit || 0,
                    include: "reviewer"
                });
                successResponse(res, 200, data)
            }
        } catch (err) {
            res.status(400)
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            await Review.destroy({
                where: {
                    id: req.params.movies_id
                }
            })
            res.status(200);
            next();
        } catch (err) {
            res.status(422)
            next(err)
        }
    }

}