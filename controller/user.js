const { User, Profile } = require('../models');
const bcrypt = require('../helpers/bcrypt');
const jwt = require('../helpers/jwt')
const imagekit = require('../lib/imagekit');


module.exports = {
    async register(req, res, next) {
        let { name, email, password } = req.body
        try {
            let isChecking = await User.findOne({
                where: { email }
            })
            if (!isChecking) {
                let user = await User.create({
                    name, email, password
                })
                let profile = await Profile.create({
                    user_id: user.id
                })
                let access_token = jwt.jwtSign({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                })
                res.status(201);
                res.data = { access_token }
                next()
            } else {
                throw new Error('Email already exist')
            }
        } catch (err) {
            // console.log(err.message);
            res.status(422)
            next(err)
        }
    },

    async login(req, res, next) {
        // console.log(req.body)
        let { email, password } = req.body
        try {
            let user = await User.findOne({
                where: { email }
            })
            if (user && user.role !== 'admin') {
                let isValidate = bcrypt.checker(password, user.password)
                if (isValidate) {
                    let access_token = jwt.jwtSign({
                        id: user.id,
                        email: user.email,
                        role: user.role
                    })
                    res.status(201);
                    res.data = { access_token }
                    next()
                }
            } else {
                res.status(404).json({ msg: "id or email is not found" })
            }
        } catch (err) {
            res.status(401);
            next(err);
        }
    },

    async userProfile(req, res, next) {
        try {
            let user = await User.findOne({
                where: {
                    id: req.payload.data.id
                },
                attributes: ['id', 'name', 'email']
            })
            if (user) {
                res.status(200);
                res.data = user;
                next();
            } else {
                throw new Error('User is not exist')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    async userUpdate(req, res, next) {
        // console.log(req.body);
        let { name, email } = req.body
        try {
            var image = await imagekit.upload({
                file: req.file.buffer,
                fileName: `IMG-${Date.now()}.jpg`
            })
            // console.log(image);

            let user = await User.update({
                name,
                email
            }, {
                where: {
                    id: req.payload.data.id
                }
            })

            let profile = await Profile.update({
                image: image.url
            }, {
                where: {
                    user_id: req.payload.data.id
                }
            })

            if (user) {
                res.status(200);
                res.data = "Successfully updated!";
                next();
            } else {
                throw new Error('Update is fail')
            }
        } catch (err) {
            res.status(422);
            next(err);
        }
    },

    // async uploadImageUser(req, res, next) {
    //     const split = req.file.originalname.split('.');
    //     const ext = split[split.length - 1];
    //     // console.log(req.file);
    //     // console.log(req.payload);
    //     try {
    //         var image = await imagekit.upload({
    //             file: req.file.buffer,
    //             fileName: `IMG-${Date.now()}.${ext}`
    //         })
    //         var user = await User.findOne({
    //             where: { id: req.payload.data.id }
    //         })
    //         console.log(user);

    //         // if (user.profile_id === null) {
    //         //     let imageDetail = await Profile.create({
    //         //         image: image.url
    //         //     })
    //         //     await User.update({
    //         //         profile_id: imageDetail.id
    //         //     }, {
    //         //         where: { id: req.payload.data.id }
    //         //     })
    //         //     res.status(200);
    //         //     res.data = "Successfully created!";
    //         //     next();
    //         // }
    //         // else if (user.profile_id !== null) {
    //         //     // console.log(image.url)
    //         //     // console.log(imageExist)  
    //         //     await Profile.update({
    //         //         image: image.url
    //         //     }, {
    //         //         where: { id: user.profile_id }
    //         //     })
    //         //     res.status(200);
    //         //     res.data = "Successfully updated!";
    //         //     next();
    //         // }
    //     } catch (err) {
    //         // console.log(err.message)
    //         res.status(422);
    //         next(err);
    //     }
    // },

    async adminLogin(req, res, next) {
        // console.log(req.body)
        let { email, password } = req.body
        try {
            let admin = await User.findOne({
                where: { email }
            })
            if (admin) {
                let isValidate = bcrypt.checker(password, admin.password)
                if (isValidate) {
                    let access_token = jwt.jwtSign({
                        id: admin.id,
                        email: admin.email,
                        role: admin.role
                    })
                    res.status(201);
                    res.data = { access_token }
                    next()
                }
            } else {
                res.status(404).json({ msg: "id or email is not found" })
            }
        } catch (err) {
            res.status(401);
            next(err);
        }
    },

}