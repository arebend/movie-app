const axios = require('axios');
const { Movie } = require('../models')
const { Op } = require('sequelize');

module.exports = {

    async adminCreate(req, res, next) {
        try {
            const data = await Movie.create({
                title: req.body.title,
                trailer: req.body.trailer,
                poster: req.body.poster,
                director: req.body.director,
                release_date: req.body.release_date,
                actor: req.body.actor,
                production_house: req.body.production_house,
                synopsis: req.body.synopsis,
                languages: req.body.languages
            })
            res.data = data
            res.status(200);
            next();
        } catch (err) {
            // console.log(err.message);
            res.status(422)
            next(err)
        }
    },


    async create(req, res, next) {
        try {
            const random = Math.ceil(Math.random() * 100)
            const movies = await axios(`https://api.themoviedb.org/3/discover/movie?api_key=${process.env.TMDB_API_KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${random}`)

            const data = await movies.data.results.forEach(async i => {
                // Fetching API
                const film = await axios(`https://api.themoviedb.org/3/movie/${i.id}?api_key=${process.env.TMDB_API_KEY}&language=en-US`)
                const movie = await axios(`https://api.themoviedb.org/3/movie/${i.id}/credits?api_key=${process.env.TMDB_API_KEY}`)
                const trailer = await axios(`https://api.themoviedb.org/3/movie/${i.id}/videos?api_key=${process.env.TMDB_API_KEY}&language=en-US`)

                // Search Director
                let containerDirector = []
                await movie.data.crew.forEach(async function (entry) {
                    if (entry.job === 'Director') {
                        containerDirector.push(entry.name)
                    }
                });

                // Search Actors
                let containerActor = [];
                await movie.data.cast.forEach(async entry => containerActor.push(entry.name))

                // GET Key Of Videos
                let key = [];
                await trailer.data.results.forEach(i => key.push(i.key))

                await Movie.create({
                    title: i.title,
                    synopsis: i.overview,
                    poster: `http://image.tmdb.org/t/p/w300${i.poster_path}`,
                    trailer: `https://www.youtube.com/watch?v=${key[0]}`,
                    release_date: i.release_date,
                    genre: film.data.genres[0].name,
                    running_time: film.data.runtime,
                    languages: film.data.spoken_languages[0].name,
                    production_house: film.data.production_companies[0].name,
                    country: film.data.production_countries[0].name,
                    director: containerDirector.join(','),
                    actor: containerActor.join(',')
                })
            });
            res.data = data
            res.status(201);
            next();
        } catch (err) {
            res.status(422)
            next(err)
        }
    },


    async showAll(req, res, next) {
        try {
            const data = await Movie.findAndCountAll({
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0,
            });
            res.data = data
            res.status(200);
            next()
        } catch (err) {
            // console.log(err.message);
            res.status(422)
            next(err)
        }
    },


    async detail(req, res, next) {
        const detail = await Movie.findByPk(req.params.id)
        res.data = detail
        res.status(200)
        next()
    },


    async search(req, res, next) {
        const movie = req.query.movie
        if (movie) {
            const data = await Movie.findAll({
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0,
                where: {
                    [Op.or]: {
                        title: {
                            [Op.like]: `%${movie}%`
                        },
                        genre: {
                            [Op.like]: `%${movie}%`
                        },
                        director: {
                            [Op.like]: `%${movie}%`
                        },
                        actor: {
                            [Op.like]: `%${movie}%`
                        },
                        production_house: {
                            [Op.like]: `%${movie}%`
                        },
                        languages: {
                            [Op.like]: `%${movie}%`
                        },
                        release_date: {
                            [Op.like]: `%${movie}%`
                        }
                    }
                }
            })
            res.data = data
            res.status(200);
            next()
        } else {
            const data = await Movie.findAll({
                limit: req.query.limit || 10,
                offset: (req.query.page - 1) * req.query.limit || 0
            })
            res.data = data
            res.status(200);
            next()
        }
    },

}