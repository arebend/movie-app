require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const router = require('./router');
const exception = require('./middlewares/exceptions')

app.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: "test movie app"
    })
})

app.use(morgan('tiny'))
app.use(cors());
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(router);

exception.forEach(handler =>
    app.use(handler)
);

module.exports = app;