'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      trailer: {
        type: Sequelize.TEXT
      },
      poster: {
        type: Sequelize.TEXT
      },
      director: {
        type: Sequelize.TEXT
      },
      genre: {
        type: Sequelize.TEXT
      },
      release_date: {
        type: Sequelize.STRING
      },
      actor: {
        type: Sequelize.TEXT
      },
      production_house: {
        type: Sequelize.STRING
      },
      synopsis: {
        type: Sequelize.TEXT
      },
      languages: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Movies');
  }
};