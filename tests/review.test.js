const server = require('../server');
const supertest = require('supertest');
const request = supertest(server);
const db = require('../models');
const { User } = require('../models')
const jwt = require('../helpers/jwt')

describe('Review API Collection', () => {

    let access_token;
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews", "Movies" RESTART IDENTITY');
        User.create({
            name: 'user_example',
            email: 'user_example@mail.com',
            password: "123456"
        })
            .then(user => {
                access_token = jwt.jwtSign({
                    id: user.dataValues.id,
                    email: user.dataValues.email,
                    role: user.dataValues.role,
                })
                done();
            })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews", "Movies" RESTART IDENTITY');
    })


    describe('POST movie/create ', () => {
        test('Should Succesfully create new Movie ', done => {
            request.post('/movie/create/admin')
                .set('Content-Type', 'application/json')
                .send({
                    title: "Avengers: Endgame",
                    trailer: "https://www.youtube.com/watch?v=TcMBFSGVi1c",
                    poster: "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
                    director: " Anthony Russo, Joe Russo",
                    release_date: "24 April 2019",
                    actor: "Robert Downey, Jr. and others",
                    production_house: "Marvel Studios",
                    synopsis: "Avengers: Endgame.synopsis",
                    languages: "English"
                }).then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success')
                    done()
                })
        })
    })


    describe('Review movie/review/create/:movies_id', () => {
        test('Should succesfully create new user', done => {
            request.post('/movie/review/create/1')
                .set("token", access_token)
                .set("Content-Type", "application/json")
                .send({ rating: 1, comment: 'bad', })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should not create new user', done => {
            request.post('/movie/review/create/1')
                .set("Content-Type", "application/json")
                .set("token", access_token)
                .send({ rating: null, comment: null, })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('GET /movie/review/show/:movies_id', () => {
        test('Should succesfully profile', done => {
            request.get('/movie/review/show/1')
                .set("Content-Type", "application/json")
                .set("token", access_token)
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('DELETE /movie/review/delete/:movies_id', () => {
        test('Should succesfully delete todo by id', done => {
            request.delete('/movie/review/delete/1')
                .set("Content-Type", "application/json")
                .set("token", access_token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

})