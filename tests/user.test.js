const server = require('../server');
const supertest = require('supertest');
const request = supertest(server);
const db = require('../models');

describe('Users API Collection', () => {

    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews", "Movies" RESTART IDENTITY');
    })
    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews", "Movies" RESTART IDENTITY');
    })

    let token;
    /* user/register */
    describe('POST /user/register', () => {
        test('Should succesfully create new user', done => {
            request.post('/user/register')
                .set("Content-Type", "application/json")
                .send({ name: 'test', email: 'test@mail.com', password: 'test' })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    token = res.body.data.access_token
                    done();
                })
        })
        test('Should not create new user', done => {
            request.post('/user/register')
                .set("Content-Type", "application/json")
                .field({ email: 'tester@mail.com', password: 'tester' })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    /* user/login */
    describe('POST /user/login', () => {
        test('Should succesfully login', done => {
            request.post('/user/login')
                .set("Content-Type", "application/json")
                .send({ email: 'test@mail.com', password: 'test' })
                .then(res => {
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should not succesfully login', done => {
            request.post('/user/login')
                .set("Content-Type", "application/json")
                .field({ email: 'admin@mail.com', password: 'admin' })
                .then(res => {
                    expect(res.statusCode).toEqual(401);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
    /* user/profile */
    describe('GET /user/profile', () => {
        test('Should succesfully profile', done => {
            request.get('/user/profile')
                .set("Content-Type", "application/json")
                .set("token", token)
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })
    /* user/updateProfile */
    describe('PUT /user/updateProfile', () => {
        test('Should succesfully updateProfile', done => {
            request.put('/user/updateProfile')
                .set("Content-Type", "application/json")
                .set("token", token)
                /* attach harus dgn filed, tdk bisa .send */
                .attach("image", './lib/sample_image/ayam.jpg')
                .field({ name: 'test', email: 'test@mail.com' })
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
        test('Should not succesfully updateProfile', done => {
            request.put('/user/updateProfile')
                .set("Content-Type", "application/json")
                .set("token", token)
                .send({ name: null, email: null })
                .then(res => {
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })
})