const server = require('../server');
const supertest = require('supertest');
const request = supertest(server);
const db = require('../models');

describe('Movies API Collection', () => {

    beforeAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews","Movies" RESTART IDENTITY');
    })
    afterAll(() => {
        db.sequelize.query('TRUNCATE "Users", "Profiles", "Reviews","Movies" RESTART IDENTITY');
    })

    describe('POST movie/create ', () => {
        test('Should Succesfully create new Movie ', done => {
            request.post('/movie/create/admin')
                .set('Content-Type', 'application/json')
                .send({
                    title: "Avengers: Endgame",
                    trailer: "https://www.youtube.com/watch?v=TcMBFSGVi1c",
                    poster: "https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_SY1000_CR0,0,674,1000_AL_.jpg",
                    director: " Anthony Russo, Joe Russo",
                    release_date: "24 April 2019",
                    actor: "Robert Downey, Jr. and others",
                    production_house: "Marvel Studios",
                    synopsis: "Avengers: Endgame.synopsis",
                    languages: "English"
                }).then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success')
                    done()
                })
        })
    })

    describe('GET /movie/show', () => {
        test('Should succesfully get Movie', done => {
            request.get('/movie/show?page=1&limit=10')
                .set("Content-Type", "application/json")
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done()
                })
        })
    })
    describe('GET /movie/show/detail/:id', () => {
        test('Should succesfully get Movie', done => {
            request.get('/movie/show/detail/1')
                .set("Content-Type", "application/json")
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done()
                })
        })
    })
    describe('GET /movie/show/search', () => {
        test('Should succesfully get Movie', done => {
            request.get('/movie/show/search')
                .set("Content-Type", "application/json")
                .then(res => {
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done()
                })
        })
    })
})
