const bcrypt = require('bcrypt');
const saltRounds = 5;

exports.hasher = (password) => {
    return bcrypt.hashSync(password, saltRounds);
}

exports.checker = (password, hash) => {
    return bcrypt.compareSync(password, hash);
}