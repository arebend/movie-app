module.exports = (req, res, next) => {
    try {
        if (req.payload.data.role === 'admin') {
            next()
        } else {
            res.status(401).json({ msg: "unauthorized" })
        }
    } catch (err) {
        res.status(500).json(err)
    }
}