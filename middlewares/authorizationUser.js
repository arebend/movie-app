const { User } = require('../models')

module.exports = (req, res, next) => {
    let id = {
        where: {
            id: req.payload.data.id
        }
    }
    User.findOne(id)
        .then(user => {
            if (user) {
                if (user.id === req.payload.data.id) {
                    next()
                } else {
                    res.status(401).json({ msg: "unauthorized" })
                }
            } else {
                res.status(404).json({ msg: "data is not found" })
            }
        })
        .catch(err => {
            res.status(500).json(err)
            // console.log(err, 'user authorization');
        })
}